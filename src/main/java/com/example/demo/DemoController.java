package com.example.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class DemoController {
@GetMapping
public String  applicationStatus()
{
    return "welcome to AWS";
}
    @GetMapping("/{name}")
    public String welcome(@PathVariable String name)
    {
        return "Hi"+ name + " welcome to java AWS ECS example";
    }
}

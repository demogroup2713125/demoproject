FROM maven:3-amazoncorretto-21-al2023 as buildenv
COPY . .
RUN mvn clean package
FROM amazoncorretto:21-alpine3.19
COPY --from=buildenv target/demo.jar demo.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/demo.jar"]